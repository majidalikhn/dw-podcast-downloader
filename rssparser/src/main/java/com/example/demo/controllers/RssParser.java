package com.example.demo.controllers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.sun.syndication.feed.synd.SyndEnclosureImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

@Component
public class RssParser {
	
	public static String podcastUrl;
	public static String folderName;


	@Value("${PODCAST_URL}")
    public void setPodcastUrl(String url) {
    	podcastUrl = url;
    }


	public static void urlDownloader() throws Exception {	
		
	XmlReader reader = xmlProvider();
	SyndFeed feed = new SyndFeedInput().build(reader);
	for (Object ob : feed.getEntries()) {
		SyndEntry entry = (SyndEntry) ob;
		SyndEnclosureImpl child = (SyndEnclosureImpl) entry.getEnclosures().get(0);
		PodcastDownloader.downloadUsingNIO(child.getUrl(), entry.getTitle().concat(".mp3"));

      }
      
	}


	private static XmlReader xmlProvider() throws MalformedURLException, IOException {
		URL url  = new URL(podcastUrl);
		return new XmlReader(url);
	}
	


	
}